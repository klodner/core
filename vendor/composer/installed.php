<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'f9d24d07dd67148a652610b002126e1e80b11839',
    'name' => 'zotlabs/hubzilla',
  ),
  'versions' => 
  array (
    'blueimp/jquery-file-upload' => 
    array (
      'pretty_version' => 'v10.31.0',
      'version' => '10.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0740f81829698b84efe17e72501e0f420ea0d611',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.9.1',
      'version' => '0.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '283a40c901101e66de7061bd359252c013dcc43c',
    ),
    'bshaffer/oauth2-server-php' => 
    array (
      'pretty_version' => 'v1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a0c8000d4763b276919e2106f54eddda6bc50fa',
    ),
    'commerceguys/intl' => 
    array (
      'pretty_version' => 'v1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bf0beb12e37ef1a61e0d09dc66cdaa1a23e62e1',
    ),
    'desandro/imagesloaded' => 
    array (
      'pretty_version' => 'v4.1.4',
      'version' => '4.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '67c4e57453120935180c45c6820e7d3fbd2ea1f9',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'league/html-to-markdown' => 
    array (
      'pretty_version' => '4.10.0',
      'version' => '4.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0868ae7a552e809e5cd8f93ba022071640408e88',
    ),
    'lukasreschke/id3parser' => 
    array (
      'pretty_version' => 'v0.0.3',
      'version' => '0.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '62f4de76d4eaa9ea13c66dacc1f22977dace6638',
    ),
    'michelf/php-markdown' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
    ),
    'pear/text_languagedetect' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e253f26cef9a9066f53f200cc3e0684018cb5b5',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '24d93aefb2cd786b7edd9f45b554aea20b28b9b1',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd4032040a750077205918c86049aa0f43d22947',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.1.1',
      ),
    ),
    'sabre/dav' => 
    array (
      'pretty_version' => '4.1.3',
      'version' => '4.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b903eeedfbdcd6cab7935661ec6dc2d90cdf8a1e',
    ),
    'sabre/event' => 
    array (
      'pretty_version' => '5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c120bec57c17b6251a496efc82b732418b49d50a',
    ),
    'sabre/http' => 
    array (
      'pretty_version' => '5.1.1',
      'version' => '5.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0aafede6961df6195ce7a8dad49296b0aaee22e',
    ),
    'sabre/uri' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f502edffafea8d746825bd5f0b923a60fd2715ff',
    ),
    'sabre/vobject' => 
    array (
      'pretty_version' => '4.3.3',
      'version' => '4.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '58f9f9b46a1080c0130bd86f4df9a568aacb9c79',
    ),
    'sabre/xml' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3b959f821c19b36952ec4a595edd695c216bfc6',
    ),
    'simplepie/simplepie' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c68e14ca3ac84346b6e6fe3c5eedf725d0f92c6',
    ),
    'smarty/smarty' => 
    array (
      'pretty_version' => 'v3.1.36',
      'version' => '3.1.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd148f7ade295014fff77f89ee3d5b20d9d55451',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'twbs/bootstrap' => 
    array (
      'pretty_version' => 'v4.5.3',
      'version' => '4.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a716fb03f965dc0846df479e14388b1b4b93d7ce',
    ),
    'twitter/bootstrap' => 
    array (
      'replaced' => 
      array (
        0 => 'v4.5.3',
      ),
    ),
    'zotlabs/hubzilla' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'f9d24d07dd67148a652610b002126e1e80b11839',
    ),
  ),
);
